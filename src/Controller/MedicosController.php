<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class MedicosController {

    /**
     * @Route("/medicos")
     */

    public function new(Request $request): Response {
     
        $RequestBody = $request->getContent();
        $JsonData = json_decode($RequestBody);
    
        $medico = new Medico();
        $medico->crm = $JsonData->crm;
        $medico->nome = $JsonData->nome;

        return new JsonResponse($medico);

    }


}

?>